module.exports = {
	env: {
		browser: true,
		node: true,
		es6: true
	},
	// globals: {},
	parserOptions: {
		ecmaVersion: 2018, // Allows for the parsing of modern ECMAScript features
		// Allows for the use of imports
		sourceType: 'module'
	},
	// The following parsers are compatible with ESLint:
	// - Esprima [default]
	// - @babel/eslint-parser - A wrapper around the Babel parser that makes it compatible with ESLint.
	// - @typescript-eslint/parser - A parser that converts TypeScript into an ESTree-compatible form so it can be used in ESLint.
	// parser: '@typescript-eslint/parser', // Specifies the ESLint parser
	// extends: [
	// Enables eslint-plugin-prettier and eslint-config-prettier. This will display prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
	// 'plugin:prettier/recommended'
	// ],
	rules: {
		semi: ['always', 'never'],
		quotes: [
			'error',
			'single',
			{
				avoidEscape: true,
				allowTemplateLiterals: true
			}
		],
		'no-unused-vars': 2,
		'sort-vars': ['error', {ignoreCase: true}],
		'array-bracket-spacing': ['error', 'never'],
		'arrow-body-style': ['error', 'as-needed'],
		'for-direction': 'error',
		'no-trailing-spaces': 'error',
		'comma-dangle': ['error', 'never'],
		'wrap-regex': 'off',
		'no-extra-parens': 'error',
		'no-new-wrappers': 'error',
		'no-mixed-spaces-and-tabs': 'error',
		'no-tabs': ['error', {allowIndentationTabs: true}],
		indent: [
			'error',
			'tab',
			{
				SwitchCase: 1,
				VariableDeclarator: 'first',
				FunctionDeclaration: {parameters: 'first'},
				ArrayExpression: 1,
				ObjectExpression: 1,
				ImportDeclaration: 1,
				flatTernaryExpressions: false
			}
		],
		'max-len': [
			'error',
			{
				code: 900,  // Is there a way to allow infinitely long lines?
				// ignoreComments: true,
				// ignoreTrailingComments: true,
				// ignoreUrls: true,
				// ignoreStrings: true,
				// ignoreTemplateLiterals: true,
				// ignoreRegExpLiterals: true,
				// ignorePattern: true,
				tabWidth: 2
			}
		],
		'new-cap': 'error',
		'no-lonely-if': 'error',
		// 'space-in-brackets': ['error', 'never'],
		'computed-property-spacing': ['error', 'never'],
		'object-curly-spacing': ['error', 'never'],
		'array-bracket-spacing': ['error', 'never'],
		'keyword-spacing': ['error', {'before': true, 'after': true}],
		'spaced-comment': ['error', 'always'],
		'no-unreachable': 'error',
		'no-unexpected-multiline': 'error',
		'no-var': 'error'
	}
}