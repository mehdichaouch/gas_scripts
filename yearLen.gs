/* Calculate if current year is a leap one
 * Returns the length of the year in days
 */

function yearLen(year){
  var yearLen;
  if ((year%400 == 0) || ((year%100 != 0) && (year%4 == 0))){
    yearLen = 366;
  }else{
    yearLen = 365;
  }
  return yearLen;
}