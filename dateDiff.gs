// src: https://stackoverflow.com/a/15289883/3408342 [30 Dec 2017]
// a and b are javascript Date objects
// unit can be m/minute/minutes or d/day/days (default: days)
// Last updated by Tukusej’s Sirs on 26 Dec 2018
function dateDiff(a, b, unit) {
  a = new Date(2018, 1, 1, 2, 0);
  b = new Date(2018, 1, 1, 3, 0);
  switch(unit){
    case "m" :
    case "minute" :
    case "minutes" :
      unit = 60000;
      
      // Discard the time-zone information
      var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate(), a.getHours(), a.getMinutes());
      var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate(), b.getHours(), b.getMinutes());
      break;
    case "d" :
    case "day" :
    case "days" :
    default :
      unit = 86400000;
      
      // Discard the time and time-zone information.
      var utc1 = Date.UTC(a.getFullYear(), a.getMonth(), a.getDate());
      var utc2 = Date.UTC(b.getFullYear(), b.getMonth(), b.getDate());
  }
  
  var dateDiff = Math.floor(Math.abs(utc2 - utc1) / unit);
  return dateDiff;
}