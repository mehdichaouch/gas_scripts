/**
 * getCalID is a Google Apps Script function that
 * returns Google Calendar ID of input calendar name.
 * The calendar name must be of calendar that is owned
 * by the same user as this function is executed by.
 *
 * @author  Tukusej's Sirs
 */

/**
 * Get calendar ID
 *
 * @param      {string}  calName  Calendar name or ID
 * @return     {string}  Calender ID or integer 1 when cal is neither calendar name nor ID
 */
function getCalId(cal) {
	if (doesCalExist(cal) == 1 && CalendarApp.getCalendarsByName(cal)[0].toString() == 'Calendar') {
		return CalendarApp.getCalendarsByName(cal)[0].getId()
	} else if (doesCalExist(cal) == 0 && CalendarApp.getCalendarById(cal).toString()) {
		return CalendarApp.getCalendarById(cal).getId()
	} else {
		return 1
	}
}