function fillInDatesIntoVar(startDate, endDate, data, minI){
//  var startDate = new Date(2017, 4, 1);
//  var endDate = new Date(2017, 11, 31);
//  var data = createEmptyArray(columns, rows);  // createEmptyArray(columns, rows), while rows are the first number in array and column the second one
//  var minI = 1;
  var dateDiff = dateDiffInDays(startDate, endDate);
//  var data = [[],[]];
//  var data = createEmptyArray(6, dateDiff);
  var rows = dateDiff;
  var columns = 6;
  var curDate = startDate;
  var i;
  var maxI;
  
  if(rows < dateDiff){
    maxI = rows;
  }else{
    maxI = dateDiff;
  }
  Logger.log(maxI);
  
  for(i = minI; i<maxI; i++, curDate.setDate(curDate.getDate() + 1)){
    // TODO: write CET/CEST dates
//    data[i][0] = getCETorCESTDate(new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate()));
    data[i][0] = new Date(curDate.getFullYear(), curDate.getMonth(), curDate.getDate());
  }
  Logger.log(data);
 return dates;
}