/**
 * Test if a year is a leap year in Gregorian calendar
 *
 * @param year int  Year to be checked
 *
 * @return Boolean  If the year is a leap year
 */
function isLeapYear(year) {
	var result;

	if (year % 4 === 0) {
		if (year % 400 === 0) {
			result = true;
			Logger.log(result);
			return result;
		} else if (year % 100 === 0) {
			result = false;
			Logger.log(result);
			return result;
		} else {
			result = true;
			Logger.log(result);
			return result;
		}
	} else {
		result = false;
		Logger.log(result);
		return result;
	}
}