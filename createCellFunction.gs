/**
 * Creates a cell function using `SUBSTITUTE` function to remove all diacritics from a cell
 *
 * @param      {string}   sheetName  Sheet name with replacement table (default: replacement_table)
 * @param      {string}   separator  Value separator (comma or semicolon; use the character itself, not its name; default: comma)
 * @param      {boolean}  hasHeader  Indicates if the table contains a header row (default: true)
 * @param      {string}   cell       Cell name to be used (default: A2)
 * @return     {string}   Function as a string
 * @customfunction
 */
function createCellFunction(sheetName = 'replacement_table', separator = ',', hasHeader = true, cell = 'A2') {
	let ss = SpreadsheetApp.getActiveSpreadsheet()
	let sheet = ss.getSheetByName(sheetName)
	let data = sheet.getDataRange().getValues()
	let cellFunction, iStart

	if (hasHeader) {
		iStart = 1
	} else {
		iStart = 0
	}

	for (let i = iStart; i < data.length; i++) {
		let find = data[i][0]
		let replace = data[i][1]

		if (i == iStart) {
			cellFunction = 'SUBSTITUTE(' + cell + separator + '"' + find + '"' + separator + '"' + replace + '")'
		} else {
			cellFunction = 'SUBSTITUTE(' + cellFunction + separator + '"' + find + '"' + separator + '"' + replace + '")'
		}
	}

	cellFunction = '=' + cellFunction

	return cellFunction
}